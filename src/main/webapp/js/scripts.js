/**
 * Created by LarsKristian on 28.05.2015.
 */


var myApp = angular.module('hubApp', []);

myApp.controller('PCDController', ['$scope', function ($scope) {

    $scope.pcd = "MSH|^~\\&|LNI Example AHD^ECDE3D4E58532D31^EUI-64||||20130301115450.720-0500||ORU^R01^ORU_R01|" +
        "002013030111545720|P|2.6|||NE|AL|||||IHE PCD ORU-R012006^HL7^2.16.840.1.113883.9.n.m^HL7\r" +
        "PID|||28da0026bc42484^^^&1.19.6.24.109.42.1.3&ISO^PI||Piggy^Sisansarah^L.^^^^L\r" +
        "OBR|1|JOXP-PCD^LNI Example AHD^ECDE3D4E58532D31^EUI-64|" +
        "JOXP-PCD^LNI Example AHD^ECDE3D4E58532D31^EUI-64|182777000^monitoring of patient^SNOMED-CT|||" +
        "20130301115452.000-0500|20130301115455.001-0500\r" +
        "OBX|1||531981^MDC_MOC_VMS_MDS_AHD^MDC|0|||||||X|||||||ECDE3D4E58532D31^^ECDE3D4E58532D31^EUI-64\r" +
        "OBX|2|CWE|68218^MDC_ATTR_REG_CERT_DATA_AUTH_BODY^MDC|0.0.0.1|2^auth-body-continua||||||R\r" +
        "OBX|3|ST|532352^MDC_REG_CERT_DATA_CONTINUA_VERSION^MDC|0.0.0.1.1|2.0||||||R\r" +
        "OBX|4|NM|532353^MDC_REG_CERT_DATA_CONTINUA_CERT_DEV_LIST^MDC|0.0.0.1.2|4||||||R\r" +
        "OBX|5|CWE|68218^MDC_ATTR_REG_CERT_DATA_AUTH_BODY^MDC|0.0.0.2|2^auth-body-continua||||||R\r" +
        "OBX|6|CWE|532354^MDC_REG_CERT_DATA_CONTINUA_REG_STATUS^MDC|0.0.0.2.1|1^unregulated(0)||||||R\r" +
        "OBX|7|CWE|68218^MDC_ATTR_REG_CERT_DATA_AUTH_BODY^MDC|0.0.0.3|2^auth-body-continua||||||R\r" +
        "OBX|8|CWE|532355^MDC_REG_CERT_DATA_CONTINUA_AHD_CERT_LIST^MDC|0.0.0.3.1|0^observation-uploadsoap||||||R\r" +
        "OBX|9|CWE|68220^MDC_TIME_SYNC_PROTOCOL^MDC|0.0.0.4|532234^MDC_TIME_SYNC_NONE^MDC||||||R\r" +
        "OBX|10|NM|8221^MDC_TIME_SYNC_ACCURACY^MDC |0.0.0.5|120000000|264339^MDC_DIM_MICRO_SEC^MDC|||||R\r" +
        "OBX|11||528391^MDC_DEV_SPEC_PROFILE_BP^MDC|1|||||||X|||||||1234567800112233^^1234567800112233^EUI-64\r" +
        "OBX|12|ST|531970^MDC_ID_MODEL_MANUFACTURER^MDC|1.0.0.1|Lamprey Networks||||||R\r" +
        "OBX|13|ST|531969^MDC_ID_MODEL_NUMBER^MDC|1.0.0.2|Blood Pressure 1.0.0||||||R\r" +
        "OBX|14|CWE|68218^MDC_ATTR_REG_CERT_DATA_AUTH_BODY^MDC|1.0.0.3|2^auth-body-continua||||||R\r" +
        "OBX|15|ST|532352^MDC_REG_CERT_DATA_CONTINUA_VERSION^MDC|1.0.0.3.1|2.0||||||R\r" +
        "OBX|16|NM|532353^MDC_REG_CERT_DATA_CONTINUA_CERT_DEV_LIST^MDC|1.0.0.3.2|24583~8199~16391~7||||||R\r" +
        "OBX|17|CWE|68218^MDC_ATTR_REG_CERT_DATA_AUTH_BODY^MDC|1.0.0.4|2^auth-body-continua||||||R\r" +
        "OBX|18|CWE|532354^MDC_REG_CERT_DATA_CONTINUA_REG_STATUS^MDC|1.0.0.4.1|1^unregulated(0)||||||R\r" +
        "OBX|19|CWE|68219^MDC_TIME_CAP_STATE^MDC|1.0.0.5|1^mds-time-capab-real-time-clock(0)||||||R\r" +
        "OBX|20|CWE|68220^MDC_TIME_SYNC_PROTOCOL^MDC|1.0.0.6|532224^MDC_TIME_SYNC_NONE^MDC||||||R\r" +
        "OBX|21|DTM|67975^MDC_ATTR_TIME_ABS^MDC|1.0.0.7|20130301115423.00||||||R|||20130301115450.733-0500\r" +
        "OBX|22||150020^MDC_PRESS_BLD_NONINV^MDC|1.0.1|||||||X|||20130301115452.733-0500\r" +
        "OBX|23|NM|150021^MDC_PRESS_BLD_NONINV_SYS^MDC|1.0.1.1|105|266016^MDC_DIM_MMHG^MDC|||||R\r" +
        "OBX|24|NM|150022^MDC_PRESS_BLD_NONINV_DIA^MDC|1.0.1.2|70|266016^MDC_DIM_MMHG^MDC|||||R\r" +
        "OBX|25|NM|150023^MDC_PRESS_BLD_NONINV_MEAN^MDC|1.0.1.3|81.7|266016^MDC_DIM_MMHG^MDC|||||R\r" +
        "OBX|26|NM|149546^MDC_PULS_RATE_NON_INV^MDC|1.0.0.8|80|264864^MDC_DIM_BEAT_PER_MIN^MDC|||||R|||201303" +
        "01115453.733-0500\r";

    $scope.result = '';




    $scope.submitPCDPHMR = function () {
        $.ajax({
            type: "POST",
            url: "api/pcd01?phmr=true",
            contentType: "application/txt",
            dataType: "text",
            traditional: false,
            processData: false,
            data: $scope.pcd.replace('&','&amp;').replace('\r', '&#xD;\r'),
            success: function(data){
                $scope.$apply(function () {
                    $scope.result = data;
                });
            }
        });
    };
    $scope.submitPCD = function () {
        $.ajax({
            type: "POST",
            url: "api/pcd01",
            contentType: "application/txt",
            dataType: "text",
            traditional: false,
            processData: false,
            data: $scope.pcd.replace('&','&amp;').replace('\r', '&#xD;\r'),
            success: function(data){
                $scope.$apply(function () {
                    $scope.result = data;
                });
            }
        });
    };
}]);


var pcdTemplate = "MSH|^~\\&|LNI Example AHD^ECDE3D4E58532D31^EUI-64||||20130301115450.720-0500||ORU^R01^ORU_R01|" +
    "002013030111545720|P|2.6|||NE|AL|||||IHE PCD ORU-R012006^HL7^2.16.840.1.113883.9.n.m^HL7\r" +
    "PID|||28da0026bc42484^^^&1.19.6.24.109.42.1.3&ISO^PI||Piggy^Sisansarah^L.^^^^L\r" +
    "OBR|1|JOXP-PCD^LNI Example AHD^ECDE3D4E58532D31^EUI-64|" +
    "JOXP-PCD^LNI Example AHD^ECDE3D4E58532D31^EUI-64|182777000^monitoring of patient^SNOMED-CT|||" +
    "20130301115452.000-0500|20130301115455.001-0500\r" +
    "OBX|1||531981^MDC_MOC_VMS_MDS_AHD^MDC|0|||||||X|||||||ECDE3D4E58532D31^^ECDE3D4E58532D31^EUI-64\r" +
    "OBX|2|CWE|68218^MDC_ATTR_REG_CERT_DATA_AUTH_BODY^MDC|0.0.0.1|2^auth-body-continua||||||R\r" +
    "OBX|3|ST|532352^MDC_REG_CERT_DATA_CONTINUA_VERSION^MDC|0.0.0.1.1|2.0||||||R\r" +
    "OBX|4|NM|532353^MDC_REG_CERT_DATA_CONTINUA_CERT_DEV_LIST^MDC|0.0.0.1.2|4||||||R\r" +
    "OBX|5|CWE|68218^MDC_ATTR_REG_CERT_DATA_AUTH_BODY^MDC|0.0.0.2|2^auth-body-continua||||||R\r" +
    "OBX|6|CWE|532354^MDC_REG_CERT_DATA_CONTINUA_REG_STATUS^MDC|0.0.0.2.1|1^unregulated(0)||||||R\r" +
    "OBX|7|CWE|68218^MDC_ATTR_REG_CERT_DATA_AUTH_BODY^MDC|0.0.0.3|2^auth-body-continua||||||R\r" +
    "OBX|8|CWE|532355^MDC_REG_CERT_DATA_CONTINUA_AHD_CERT_LIST^MDC|0.0.0.3.1|0^observation-uploadsoap||||||R\r" +
    "OBX|9|CWE|68220^MDC_TIME_SYNC_PROTOCOL^MDC|0.0.0.4|532234^MDC_TIME_SYNC_NONE^MDC||||||R\r" +
    "OBX|10|NM|8221^MDC_TIME_SYNC_ACCURACY^MDC |0.0.0.5|120000000|264339^MDC_DIM_MICRO_SEC^MDC|||||R\r" +
    "OBX|11||528391^MDC_DEV_SPEC_PROFILE_BP^MDC|1|||||||X|||||||1234567800112233^^1234567800112233^EUI-64\r" +
    "OBX|12|ST|531970^MDC_ID_MODEL_MANUFACTURER^MDC|1.0.0.1|Lamprey Networks||||||R\r" +
    "OBX|13|ST|531969^MDC_ID_MODEL_NUMBER^MDC|1.0.0.2|Blood Pressure 1.0.0||||||R\r" +
    "OBX|14|CWE|68218^MDC_ATTR_REG_CERT_DATA_AUTH_BODY^MDC|1.0.0.3|2^auth-body-continua||||||R\r" +
    "OBX|15|ST|532352^MDC_REG_CERT_DATA_CONTINUA_VERSION^MDC|1.0.0.3.1|2.0||||||R\r" +
    "OBX|16|NM|532353^MDC_REG_CERT_DATA_CONTINUA_CERT_DEV_LIST^MDC|1.0.0.3.2|24583~8199~16391~7||||||R\r" +
    "OBX|17|CWE|68218^MDC_ATTR_REG_CERT_DATA_AUTH_BODY^MDC|1.0.0.4|2^auth-body-continua||||||R\r" +
    "OBX|18|CWE|532354^MDC_REG_CERT_DATA_CONTINUA_REG_STATUS^MDC|1.0.0.4.1|1^unregulated(0)||||||R\r" +
    "OBX|19|CWE|68219^MDC_TIME_CAP_STATE^MDC|1.0.0.5|1^mds-time-capab-real-time-clock(0)||||||R\r" +
    "OBX|20|CWE|68220^MDC_TIME_SYNC_PROTOCOL^MDC|1.0.0.6|532224^MDC_TIME_SYNC_NONE^MDC||||||R\r" +
    "OBX|21|DTM|67975^MDC_ATTR_TIME_ABS^MDC|1.0.0.7|20130301115423.00||||||R|||20130301115450.733-0500\r" +
    "OBX|22||150020^MDC_PRESS_BLD_NONINV^MDC|1.0.1|||||||X|||20130301115452.733-0500\r" +
    "OBX|23|NM|150021^MDC_PRESS_BLD_NONINV_SYS^MDC|1.0.1.1|105|266016^MDC_DIM_MMHG^MDC|||||R\r" +
    "OBX|24|NM|150022^MDC_PRESS_BLD_NONINV_DIA^MDC|1.0.1.2|70|266016^MDC_DIM_MMHG^MDC|||||R\r" +
    "OBX|25|NM|150023^MDC_PRESS_BLD_NONINV_MEAN^MDC|1.0.1.3|81.7|266016^MDC_DIM_MMHG^MDC|||||R\r" +
    "OBX|26|NM|149546^MDC_PULS_RATE_NON_INV^MDC|1.0.0.8|80|264864^MDC_DIM_BEAT_PER_MIN^MDC|||||R|||201303" +
    "01115453.733-0500\r";

function escapeHTML(html) {
    return html.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;');
}
