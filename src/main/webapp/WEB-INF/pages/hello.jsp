<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<title>Continua Test Hub</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<link href="css/bootstrap.min.css" rel="stylesheet" media="screen"/>
	<link href="css/core.css" rel="stylesheet" media="screen"/>
	<script src="http://code.jquery.com/jquery-latest.js"></script>
	<script src="js/angular.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/scripts.js"></script>
	<!--<script src="${pageContext.request.contextPath}/js/scripts.js"></script>-->
</head>
<body ng-app="hubApp">
<div class="container" ng-controller="PCDController">

	<p>
		<textarea class="form-control" id="s" name="s" placeholder="PCD01 content" rows="10" cols="140" ng-model="pcd"></textarea>
	</p>

	<p>
		<textarea class="form-control" ng-hide="result===''" id="result" name="result" placeholder="PHMR" rows="10" cols="140" ng-model="result"></textarea>
	</p>

	<p>
		<button class="btn btn-default" ng-click="submitPCD()">Submit PCD-01</button>
		<button class="btn btn-default" ng-click="submitPCDPHMR()">Submit PCD-01 - get PHMR</button>
	</p>
	<p>
		<!--<button ng-click="submitPCD()">Submit PCD-01 and return CDA/PHMR xml document</button>-->
	</p>

</div>

</body>
</html>