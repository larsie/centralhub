package bz.roland.centralhub;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HelloController {
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String printWelcome(ModelMap model) {
		model.addAttribute("message", "Hello world!");
		return "hello";
	}

	@RequestMapping(value = "/testws", method = RequestMethod.GET)
	@ResponseBody
	public String testWebService() {
		String patientID = "daac09bb0a3f45e^^^&amp;1.3.6.1.4.1.21367.2005.13.20.3000&amp;ISO";
		WebServiceClient client = new WebServiceClient();
		//client.setDefaultUri("http://xdstest1.cloudapp.net/axis2/services/xdsrepositoryb.xdsrepositorybHttpSoap12Endpoint/");
		client.setDefaultUri("http://xdstest1.cloudapp.net:8020/axis2/services/xdsrepositoryb?wsdl");
		String ret = client.simpleSendAndReceive(
				patientID,
				"d:\\test.txt",
				"application/xml"
		);
		//client.customSendAndReceive();
		return ret;
	}
}