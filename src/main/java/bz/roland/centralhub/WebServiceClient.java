
package bz.roland.centralhub;

import java.io.*;
//import javax.xml.soap.SOAPHeader;
import javax.xml.bind.Element;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;

import org.apache.axiom.om.*;
import org.apache.axiom.soap.*;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.client.core.WebServiceMessageExtractor;
import org.springframework.ws.soap.SoapHeader;
import org.springframework.ws.soap.SoapMessage;
import org.springframework.ws.soap.axiom.AxiomSoapMessage;
import org.springframework.ws.soap.axiom.AxiomSoapMessageFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.SoapVersion;
import org.springframework.ws.support.MarshallingUtils;
//import org.springframework.ws.soap.saaj.SaajSoapMessageFactory;

import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.IOException;
import java.util.UUID;

public class WebServiceClient {

    private static final Log logger = LogFactory.getLog(WebServiceClient.class);

    WebServiceClient() {
        logger.info("WebServiceClient constructor");
        // TODO: Set in spring config files
        AxiomSoapMessageFactory messageFactory = new AxiomSoapMessageFactory();
        messageFactory.setSoapVersion(SoapVersion.SOAP_12);
        //messageFactory.setAttachmentCaching(false);
        //messageFactory.setPayloadCaching(false);
        //messageFactory.setAttachmentCacheDir(new File("/tmp/"));
        messageFactory.setAttachmentCacheDir(new File("d:\\"));
        //messageFactory.afterPropertiesSet();
        webServiceTemplate = new WebServiceTemplate(messageFactory);
        //webServiceTemplate.setMessageFactory(messageFactory);
        setDefaultUri("http://xdstest1.cloudapp.net:8020/axis2/services/xdsrepositoryb?wsdl");
    }

    private WebServiceTemplate webServiceTemplate;

    public void setWebServiceTemplate(WebServiceTemplate t) {
        this.webServiceTemplate = t;
        System.out.println("Setting webservicetemplate");
    }

    public void setDefaultUri(String defaultUri) {
        webServiceTemplate.setDefaultUri(defaultUri);
    }


    public String simpleSendAndReceive(final String originalPatientId, final String filename, final String fileContentType) {

        final String patientId;
        if (originalPatientId.contains("&amp;")) {
            patientId = originalPatientId;
        } else {
            patientId = originalPatientId.replace("&", "&amp;").replace("^PI", "").trim();
        }

        logger.info("XDS request for patient:" + patientId);

        try {

            return webServiceTemplate.sendAndReceive(new WebServiceMessageCallback() {
                public void doWithMessage(WebServiceMessage message) throws IOException, TransformerException {

                    XDSSoapMessageFactory xdsMessageFactory = new XDSSoapMessageFactory();
                    xdsMessageFactory.createMessage(message, patientId, filename, fileContentType);

                }
            }, new WebServiceMessageExtractor<String>() {
                public String extractData(WebServiceMessage message) throws IOException, TransformerException {
                    AxiomSoapMessage axiomSoapMessage = (AxiomSoapMessage) message;
                    if (axiomSoapMessage.hasFault()) {
                        logger.debug("XDS returned error");
                        return "XDS error:" + axiomSoapMessage.getFaultReason();
                    } else {
                        String status = axiomSoapMessage.getDocument().getElementsByTagNameNS("urn:oasis:names:tc:ebxml-regrep:xsd:rs:3.0","RegistryResponse")
                                .item(0).getAttributes().getNamedItem("status").toString();
                        if (status.contains("Success")) {
                            logger.info("XDS returned success");
                            return "XDS Success: " + status;
                        } else {
                            logger.error("XDS returned failure");
                            return "XDS error: " + status;
                        }
                    }
                }
            });
        } catch (Exception e) {
            return "XDS Error:" + e.toString();
        }

    }

}
