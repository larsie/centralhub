package bz.roland.centralhub;

import ca.uhn.hl7v2.DefaultHapiContext;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v26.datatype.CX;
import ca.uhn.hl7v2.model.v26.group.ORU_R01_OBSERVATION;
import ca.uhn.hl7v2.model.v26.group.ORU_R01_ORDER_OBSERVATION;
import ca.uhn.hl7v2.model.v26.group.ORU_R01_PATIENT_RESULT;
import ca.uhn.hl7v2.model.v26.message.ORU_R01;
import ca.uhn.hl7v2.model.v26.segment.MSH;
import ca.uhn.hl7v2.model.v26.segment.PID;
import ca.uhn.hl7v2.parser.EncodingNotSupportedException;
import ca.uhn.hl7v2.parser.Parser;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.net4care.phmr.builders.DanishPHMRBuilder;
import org.net4care.phmr.codes.NPU;
import org.net4care.phmr.model.*;
import org.springframework.web.bind.annotation.*;
import org.w3c.dom.Document;

import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.security.Principal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

//import ca.uhn.hl7v2.model.v22.datatype.PN;
//import ca.uhn.hl7v2.model.v22.message.ADT_A01;
//import ca.uhn.hl7v2.model.v22.segment.MSH;

@RestController
public class ApiController {

    private static final Log logger = LogFactory.getLog(ApiController.class);

    @RequestMapping(value = "/api/", method = RequestMethod.GET)
    public String index(Principal principal) {

        return principal != null ? "home/homeSignedIn" : "home/homeNotSignedIn";
    }

    @RequestMapping(value = "/api/", method = RequestMethod.POST)
    public String indexPost(Principal principal) {

        return principal != null ? "home/homeSignedIn" : "home/homeNotSignedIn";
    }


    @RequestMapping(value = "/api/pcd01", method = RequestMethod.POST, produces="application/txt")
    @ResponseBody
    public String processMessage(Principal principal, HttpServletResponse response, @RequestBody String pcdPacket,
                                 @RequestParam(value = "phmr", required = false) String phmrRequest) {

        response.setContentType("application/txt");

        String pcdPacketFormatted = pcdPacket.replace("#xD;","\r").replace("&amp;", "&");

        // Set up PHMR-builder
        SimpleClinicalDocument cda = new DanishPHMRModel();

        // Set various PHMR data
        // Define the time of when the document has been created
        Calendar norwegianCalendar = Calendar.getInstance(new Locale("no-NO"));
        norwegianCalendar.set(2014, 8, 8, 14, 23, 00);
        Date documentCreationTime = norwegianCalendar.getTime();

        cda.setEffectiveTime(documentCreationTime);

        // Set the document version as part of the header
        cda.setDocumentVersion("2358344", 1);

		/*
           * The HapiContext holds all configuration and provides factory methods for obtaining
           * all sorts of HAPI objects, e.g. parsers.
           */
        HapiContext context = new DefaultHapiContext();

        Parser p = context.getGenericParser();

        Message hapiMsg;
        try {
            // The parse method performs the actual parsing
            hapiMsg = p.parse(pcdPacketFormatted);
        } catch (EncodingNotSupportedException e) {
            e.printStackTrace();
            return e.toString();
        } catch (HL7Exception e) {
            e.printStackTrace();
            return e.toString();
        }

        ORU_R01 oruMsg = (ORU_R01) hapiMsg;
        MSH msh = oruMsg.getMSH();

        try {
            logger.info("Trying to find patient"+oruMsg.printStructure());
        } catch (HL7Exception e) {
            e.printStackTrace();
        }

        // Patient ID, name, address etc.
        ORU_R01_PATIENT_RESULT patientResult = oruMsg.getPATIENT_RESULT();
        //String patientId = patientResult.getPATIENT().getPID().getPatientID().getIDNumber().toString();
        PID pid = patientResult.getPATIENT().getPID();
        CX[] cx = pid.getPid3_PatientIdentifierList();
        String patientId = "unknown";

        try  {
            logger.info("Handling message for patient="+cx[0].toString());
            patientId = cx[0].toString().replace("CX[","").replace("]","").trim();
        } catch (Exception e) {
            logger.error("Exception while getting patient id: "+e.toString());
            return "Error while getting patient id "+e;
        }

        logger.info("Handling message for patientid="+patientId);

        AddressData citizenAddress = new AddressData.AddressBuilder("0755", "Oslo").
                setCountry("Norway").
                addAddressLine("Innbyggerveien 85F").
                setUse(AddressData.Use.HomeAddress).build();
        PersonIdentity patient =
                new PersonIdentity.PersonBuilder("Nordmann").
                        addGivenName("Ola").
                        setGender(PersonIdentity.Gender.Male).
                        setPersonID(patientId).
                        setAddress(citizenAddress).
                        addTelecom(AddressData.Use.HomeAddress, "tel", "123456789").
                        setBirthTime(1973, Calendar.MARCH, 2).build();
        cda.setPatient(patient);

        // Enter measurement data
        String retstr = processMeasurementInformation(patientResult.getORDER_OBSERVATION(), cda);

        // Create a builder for PHMR as XML
        DanishPHMRBuilder phmrBuilder = new DanishPHMRBuilder();
        // Ask the cda to construct a representation of itself using this builder
        cda.construct(phmrBuilder);

        // Extract the actual XML DOM
        Document phmrAsXML = phmrBuilder.getDocument();
        // and the String representation
        String phmrAsString = phmrBuilder.getDocumentAsString();

        try {
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            Result output = new StreamResult(new File("d:\\phmr.xml"));
            Source input = new DOMSource(phmrAsXML);
            transformer.transform(input, output);
        } catch (Exception e) {
            logger.info("Exception while saving document: "+e);
        }

        //return "<p>"+retstr+"</p><hr/><pre>"+phmrAsString+"</pre>";

        // Submit to XDS
        //String patientID = "daac09bb0a3f45e^^^&amp;1.3.6.1.4.1.21367.2005.13.20.3000&amp;ISO";
        WebServiceClient client = new WebServiceClient();
        String soapRetMessage = client.simpleSendAndReceive(
                patientId,
                "d:\\phmr.xml",
                "text/xml"
        );

        if (phmrRequest != null && phmrRequest.equals("true")) {
            return phmrAsString;
        }
        else {
            return soapRetMessage;
        }

    }

    private String processMeasurementInformation(ORU_R01_ORDER_OBSERVATION order_observation, SimpleClinicalDocument cda) {

        // a) when was the measurement made
        Calendar norwegianCalendar = Calendar.getInstance(new Locale("no-NO"));
        norwegianCalendar.set(2014, 8, 7, 13, 23, 00);
        Date timeOfMeasurement1 = norwegianCalendar.getTime();

        // b) what was the context - here the patient himself made it and manually
        // entered the measured weight into the home device
        Context context = new Context(Context.ProvisionMethod.TypedByCitizen, Context.PerformerType.Citizen);

        String retstr = "";

        try {
            retstr += "<p>observation="+order_observation.toString()+"</p>";

            List<ORU_R01_OBSERVATION> obs = order_observation.getOBSERVATIONAll();
            retstr += "<p>obs number="+obs.size()+"</p>";

            for(ORU_R01_OBSERVATION ob : obs) {
                //retstr += "<p>ob2 "+ ob.getOBX().getObx2_ValueType() +",3 "+ob.getOBX().getObx3_ObservationIdentifier()+" ="+ Arrays.toString(ob.getOBX().getObservationValue()) +"</p>";
                //retstr += "<p>"+ob.getOBX().getObx3_ObservationIdentifier().getComponent(2)+"</p>";
                String value = ob.getOBX().getObservationValue(0).getData().encode();
                String sensor = ob.getOBX().getObx3_ObservationIdentifier().getComponent(1).toString();
                //retstr += "<p>sensor="+sensor+", value="+value+"</p>";
                //retstr += "<p>"+sensor+"</p>";
                boolean bloodPressure = false;
                float bpSys = -1;
                float bpDia = -1;
                float bpMean = -1;
                float bpPulse = -1;
                if (sensor.contains("MDC_PRESS_BLD_NONINV_SYS")) {
                    // Sys blood pressure
                    //retstr += "<p>bp sys= "+value+"</p>";
                    Measurement bp = NPU.createBloodPresureSystolic(value, timeOfMeasurement1, context);
                    cda.addResult(bp);
                } else if (sensor.contains("MDC_PRESS_BLD_NONINV_DIA")) {
                    // Dia blood pressure
                    Measurement bp = NPU.createBloodPresureDiastolic(value, timeOfMeasurement1, context);
                    cda.addResult(bp);
                } else if (sensor.contains("MDC_ID_MODEL_MANUFACTURER")) {
                    // Dia blood pressure
                    MedicalEquipment equipment = new MedicalEquipment.MedicalEquipmentBuilder().
                            setMedicalDeviceCode("N/A").
                            setMedicalDeviceDisplayName("Blood pressure").
                            setManufacturerModelName("Manufacturer: "+value).
                            setSoftwareName("N/A").
                            build();
                    cda.addMedicalEquipment(equipment);
                }
                /*else if (sensor.contains("MDC_PRESS_BLD_NONINV_MEAN")) {
                    // Mean blood pressure
                    Measurement bp = NPU.createBloo(value, timeOfMeasurement1, context);
                    cda.addResult(bp);
                } else if (sensor.contains("MDC_PULS_RATE_NON_INV")) {
                    // Pulse
                    Measurement bp = NPU.createBloodPresureSystolic(value, timeOfMeasurement1, context);
                    cda.addResult(bp);
                }*/
            }

            //ORU_R01_OBSERVATION ob = observation.getOBSERVATION();
            //retstr += "<p>ob tostring="+ Arrays.toString(ob.getOBX().getObservationValue()) +"</p>";

        }
        catch (HL7Exception e) {
            System.out.println(e.toString());
        }

    /*
        Measurement weightAtTime1 = NPU.createWeight("91.1", timeOfMeasurement1, context);
        System.out.println(" -> Built a measurement: " + weightAtTime1.toString());

        cda.addResult(weightAtTime1);

        // Also - define the device type used for making the measurement
        MedicalEquipment equipment = new MedicalEquipment.MedicalEquipmentBuilder().
                setMedicalDeviceCode("EPQ12225").
                setMedicalDeviceDisplayName("Weight").
                setManufacturerModelName("Manufacturer: AD Company / Model: 6121ABT1").
                setSoftwareName("SerialNr: 6121ABT1-987 Rev. 3 / SW Rev. 20144711").
                build();
        System.out.println(" -> Built a device: " + equipment.toString());

        cda.addMedicalEquipment(equipment);
        */

        return retstr;
    }

}
