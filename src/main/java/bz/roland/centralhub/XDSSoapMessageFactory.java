package bz.roland.centralhub;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.om.OMOutputFormat;
import org.apache.axiom.om.OMText;
import org.apache.axiom.soap.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.soap.axiom.AxiomSoapMessage;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import java.util.Random;
import java.util.UUID;

/**
 * Created by LarsKristian on 05.06.2015.
 */
public class XDSSoapMessageFactory {

    private static final Log logger = LogFactory.getLog(XDSSoapMessageFactory.class);
    Random rand = new Random();

    String generateRandomId()
    {
        String id = "urn:uuid:"+UUID.randomUUID().toString();
        logger.info("Creating random id:" +id);
        return id;
    }

    void createMessage(WebServiceMessage message, String patientID, String attachMentFileName, String attachmentContentType) {

        // TODO: This needs to be done some other way
        String documentUUID = generateRandomId();
        String registryObjectID = generateRandomId();
        String messageUUID = generateRandomId();
        String hl7DocumentId = "1.3.6.1.4.1.12559.11.1.2.2.1.1.3."+rand.nextInt(0xffff);

        AxiomSoapMessage soapMessage = (AxiomSoapMessage) message;
        SOAPMessage axiomMessage = soapMessage.getAxiomMessage();
        SOAPFactory factory = (SOAPFactory) axiomMessage.getOMFactory();
        SOAPBody body = axiomMessage.getSOAPEnvelope().getBody();
        SOAPHeader header = axiomMessage.getSOAPEnvelope().getHeader();

        // Defining name spaces
        OMNamespace ns =
                factory.createOMNamespace("http://www.springframework.org/spring-ws/samples/mtom", "tns");
        OMNamespace a =
                factory.createOMNamespace("http://www.w3.org/2005/08/addressing", "a");
        OMNamespace xdsb =
                factory.createOMNamespace("urn:ihe:iti:xds-b:2007", "xdsb");
        OMNamespace lcm =
                factory.createOMNamespace("urn:oasis:names:tc:ebxml-regrep:xsd:lcm:3.0", "lcm");
        OMNamespace rim =
                factory.createOMNamespace("urn:oasis:names:tc:ebxml-regrep:xsd:rim:3.0", "rim");

        OMNamespace soapenv = factory.createOMNamespace("http://www.w3.org/2003/05/soap-envelope", "soapenv");
        soapMessage.setSoapAction("urn:ihe:iti:2007:ProvideAndRegisterDocumentSet-b");
        axiomMessage.getSOAPEnvelope().setNamespace(soapenv);

        // Setting SOAP header values
        SOAPHeaderBlock action = header.addHeaderBlock("Action", a);
        action.setMustUnderstand(true);
        action.setText("urn:ihe:iti:2007:ProvideAndRegisterDocumentSet-b");

        SOAPHeaderBlock address = header.addHeaderBlock("Address", a);
        address.setText("http://www.w3.org/2005/08/addressing/anonymous");

        SOAPHeaderBlock messageID = header.addHeaderBlock("MessageID", a);
        messageID.setText(messageUUID);

        SOAPHeaderBlock to = header.addHeaderBlock("To", a);
        to.setMustUnderstand(true);
        to.setText("http://xdstest1.cloudapp.net:8020/axis2/services/xdsrepositoryb?wsdl");

        // Defining the body of the request

        OMElement provideAndRegisterElement =
                factory.createOMElement("ProvideAndRegisterDocumentSetRequest", xdsb);
        body.addChild(provideAndRegisterElement);

        OMElement submitObjectsRequest = factory.createOMElement("SubmitObjectsRequest", lcm);
        provideAndRegisterElement.addChild(submitObjectsRequest);

        // Defining the ebxml content

        OMElement registryObjectList = factory.createOMElement("RegistryObjectList", rim);

        OMElement registryPackage = createRegistryPackage(
                registryObjectID, //String id,
                "urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:RegistryPackage", //String objectType,
                "20150409093855", //String submissionTime,
                factory,
                rim);

        registryPackage.addChild(createClassification(
                "urn:uuid:aa543740-bdda-424e-8c96-df4873be8500", // String classificationScheme,
                registryObjectID, // String classifiedObject,
                generateRandomId(), // String id,
                "11488-4", //String nodeRepresentation,
                "urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification", //String objectType,
                "2.16.840.1.113883.6.1", // String codingScheme,
                "Consultation Note",
                factory, rim));

        /*
        <rim:Classification classificationNode="urn:uuid:a54d6aa5-d40d-43f9-88c5-b4633d873bdd"
                                      classifiedObject="urn:uuid:72a31a99-9569-4114-b122-7ffd9255a96b"
                                      id="urn:uuid:9648a6ee-1ec6-4c3a-bd6f-7bcfe2e4386b"
                                      objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification"/>
         */

        OMElement classificationNode = factory.createOMElement("Classification", rim);
        classificationNode.addAttribute("classificationNode", "urn:uuid:a54d6aa5-d40d-43f9-88c5-b4633d873bdd", null);
        classificationNode.addAttribute("classifiedObject", registryObjectID, null);
        classificationNode.addAttribute("id", generateRandomId(), null);
        classificationNode.addAttribute("objectType", "urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification", null);
        registryPackage.addChild(classificationNode);

        registryPackage.addChild(createExternalIdentifier(
                generateRandomId(), //String id,
                "urn:uuid:554ac39e-e3fe-47fe-b233-965d2a147832", //String identificationScheme,
                "urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:ExternalIdentifier", //String objectType,
                registryObjectID, //String registryObject,
                "1.3.6.1.4.1.21367.2012.2.1.1", //String value,
                "XDSSubmissionSet.sourceId", //name
                factory, rim));

        registryPackage.addChild(createExternalIdentifier(
                generateRandomId(), //String id,
                "urn:uuid:96fdda7c-d067-4183-912e-bf5ee74998a8", //String identificationScheme,
                "urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:ExternalIdentifier", //String objectType,
                registryObjectID, //String registryObject,
                "1.3.6.1.4.1.21367.2012.2.1.1", //String value,
                "XDSSubmissionSet.uniqueId",
                factory, rim));

        registryPackage.addChild(createExternalIdentifier(
                generateRandomId(), //String id,
                "urn:uuid:6b5aea1a-874d-4603-a4bc-96a0a7b38446", //String identificationScheme,
                "urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:ExternalIdentifier", //String objectType,
                registryObjectID, //String registryObject,
                patientID, //String value,
                "XDSSubmissionSet.patientId",
                factory,rim));

        registryObjectList.addChild(registryPackage);
        submitObjectsRequest.addChild(registryObjectList);

        // Extrinsic object

        OMElement extrinsicObject = createExtrinsicObject(
                documentUUID, // id
                attachmentContentType, // content type
                "urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1", // objectType
                "urn:oasis:names:tc:ebxml-regrep:StatusType:Approved", //String status,
                "20150412112333",
                patientID,
                factory, rim);

        extrinsicObject.addChild(createClassification(
                "urn:uuid:41a5887f-8865-4c09-adf7-e362475b143a", //String classificationScheme,
                documentUUID, //String classifiedObject,
                generateRandomId(), //String id,
                "DEMO-Consult" , //String nodeRepresentation,
                "urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification", //String objectType,
                "1.3.6.1.4.1.21367.100.1", //String codingScheme,
                "Consultation",// nameValue
                factory, rim));
        extrinsicObject.addChild(createClassification(
                "urn:uuid:f4f85eac-e6cb-4883-b524-f2705394840f", //String classificationScheme,
                documentUUID, //String classifiedObject,
                generateRandomId(), //String id,
                "N"  , //String nodeRepresentation,
                "urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification", //String objectType,
                "2.16.840.1.113883.5.25", //String codingScheme,
                "Normal",// nameValue
                factory, rim));
        extrinsicObject.addChild(createClassification(
                "urn:uuid:a09d5840-386c-46f2-b5ad-9c3699a4309d", //String classificationScheme,
                documentUUID, //String classifiedObject,
                generateRandomId(), //String id,
                "urn:ihe:rad:TEXT"  , //String nodeRepresentation,
                "urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification", //String objectType,
                "1.3.6.1.4.1.19376.1.2.3", //String codingScheme,
                "urn:ihe:rad:TEXT",// nameValue
                factory, rim));
        extrinsicObject.addChild(createClassification(
                "urn:uuid:f33fb8ac-18af-42cc-ae0e-ed0b0bdb91e1", //String classificationScheme,
                documentUUID, //String classifiedObject,
                generateRandomId(), //String id,
                "ACC"   , //String nodeRepresentation,
                "urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification", //String objectType,
                "2.16.840.1.113883.5.11", //String codingScheme,
                "Accident Site",// nameValue
                factory, rim));
        extrinsicObject.addChild(createClassification(
                "urn:uuid:cccf5598-8b07-4b77-a05e-ae952c785ead", //String classificationScheme,
                documentUUID, //String classifiedObject,
                generateRandomId(), //String id,
                "Emergency"    , //String nodeRepresentation,
                "urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification", //String objectType,
                "Connect-a-thon practiceSettingCodes", //String codingScheme,
                "Emergency",// nameValue
                factory, rim));
        extrinsicObject.addChild(createClassification(
                "urn:uuid:f0306f51-975f-434e-a61c-c59651d33983", //String classificationScheme,
                documentUUID, //String classifiedObject,
                generateRandomId(), //String id,
                "11488-4"    , //String nodeRepresentation,
                "urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification", //String objectType,
                "2.16.840.1.113883.6.1", //String codingScheme,
                "Consultation",// nameValue
                factory, rim));
        extrinsicObject.addChild(createExternalIdentifier(
                generateRandomId(), //String id,
                "urn:uuid:2e82c1f6-a085-4c72-9da3-8640a32e42ab", //String identificationScheme,
                "urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:ExternalIdentifier", //String objectType,
                documentUUID, //String registryObject,
                hl7DocumentId, //String value,
                "XDSDocumentEntry.uniqueId",
                factory, rim));
        extrinsicObject.addChild(createExternalIdentifier(
                generateRandomId(), //String id,
                "urn:uuid:58a6f841-87b3-4a3e-92fd-a8ffeff98427", //String identificationScheme,
                "urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:ExternalIdentifier", //String objectType,
                documentUUID, //String registryObject,
                patientID, //String value,
                "XDSDocumentEntry.patientId",
                factory, rim));
        registryObjectList.addChild(extrinsicObject);

        // Association
        OMElement association = createAssociation(
                "urn:oasis:names:tc:ebxml-regrep:AssociationType:HasMember", //String associationType,
                generateRandomId(), //String id,
                registryObjectID, //String sourceObject,
                documentUUID, //String targetObject,
                "Original", //String submissionSetStatus,
                factory, rim);
        registryObjectList.addChild(association);

                /*
                <xdsb:Document id="urn:uuid:4fe5af4f-f564-44cc-beef-0c742296c48f">
                <xop:Include href="cid:1.urn:uuid:4FE5AF4FF56444CCBEEF0C742296C48F@ws.jboss.org"></xop:Include></xdsb:Document> */

        // Defining the attachment

        OMElement document = factory.createOMElement("Document", xdsb);
        document.addAttribute("id",documentUUID, null);
        provideAndRegisterElement.addChild(document);
        //DataSource dataSource = new FileDataSource("/tmp/test.txt");
        DataSource dataSource = new FileDataSource(attachMentFileName);

        DataHandler dataHandler = new DataHandler(dataSource);
        OMText text = factory.createOMText(dataHandler, true);

        document.addChild(text);

        try {
            logger.info("XML:"+axiomMessage.getSOAPEnvelope().toStringWithConsume());
        } catch (Exception e) {
            logger.info("While outputting xml: "+e);
        }


        OMOutputFormat outputFormat = new OMOutputFormat();
        outputFormat.setSOAP11(false);
        outputFormat.setDoOptimize(true);
        soapMessage.setOutputFormat(outputFormat);
    }

    public OMElement createRegistryPackage(String id,
                                           String objectType,
                                           String submissionTime,
                                           SOAPFactory factory,
                                           OMNamespace ns) {

         /*
        <rim:RegistryPackage id="urn:uuid:72a31a99-9569-4114-b122-7ffd9255a96b"
                            objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:RegistryPackage">
          <rim:Slot name="submissionTime">
            <rim:ValueList>
                <rim:Value>20150409093855</rim:Value>
            </rim:ValueList>
          </rim:Slot>
          <rim:Name>
            <rim:LocalizedString value="XDS Submission Set"/>
          </rim:Name>
          <rim:Classification...
         */

        OMElement element = factory.createOMElement("RegistryPackage", ns);
        if (id != null) {
            element.addAttribute("id", id, null);
        }
        if (objectType != null) {
            element.addAttribute("objectType", objectType, null);
        }

        element.addChild(createSlot("submissionTime",submissionTime,factory,ns));
        element.addChild(createName("XDS Submission Set", factory, ns));

        return element;

    }



    public OMElement createExternalIdentifier(
            String id,
            String identificationScheme,
            String objectType,
            String registryObject,
            String value,
            String nameValue,
            SOAPFactory factory,
            OMNamespace ns) {

        /*
        <rim:ExternalIdentifier id="urn:uuid:bcfcb9f9-9bd6-493d-b52f-15062078f3db"
                          identificationScheme="urn:uuid:554ac39e-e3fe-47fe-b233-965d2a147832"
                          objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:ExternalIdentifier"
                          registryObject="urn:uuid:72a31a99-9569-4114-b122-7ffd9255a96b"
                          value="1.3.6.1.4.1.21367.2012.2.1.1">
                <rim:Name>
                    <rim:LocalizedString value="XDSSubmissionSet.sourceId"/>
                </rim:Name>
              </rim:ExternalIdentifier>
         */


        OMElement element = factory.createOMElement("ExternalIdentifier", ns);
        if (id != null) {
            element.addAttribute("id", id, null);
        }
        if (identificationScheme != null) {
            element.addAttribute("identificationScheme", identificationScheme, null);
        }
        if (objectType != null) {
            element.addAttribute("objectType", objectType, null);
        }

        if (registryObject != null) {
            element.addAttribute("registryObject", registryObject, null);
        }
        if (value != null) {
            element.addAttribute("value", value, null);
        }

        element.addChild(createName(nameValue, factory, ns));

        return element;
    }


    public OMElement createClassification(String classificationScheme,
                                          String classifiedObject,
                                          String id,
                                          String nodeRepresentation,
                                          String objectType,
                                          String codingScheme,
                                          String nameValue,
                                          SOAPFactory factory,
                                          OMNamespace ns) {
        /*
        <rim:Classification classificationScheme="urn:uuid:f4f85eac-e6cb-4883-b524-f2705394840f"
                  classifiedObject="urn:uuid:f269dfd6-b6a9-412c-b37a-50617d6e068a"
                  id="urn:uuid:951c1752-21e6-489f-ad9c-62ad3811d6aa"
                  nodeRepresentation="N"
                  objectType="urn:oasis:names:tc:ebxml-regrep:ObjectType:RegistryObject:Classification">
            <rim:Slot name="codingScheme">
                <rim:ValueList>
                    <rim:Value>2.16.840.1.113883.5.25</rim:Value>
                </rim:ValueList>
            </rim:Slot>
            <rim:Name>
                <rim:LocalizedString value="Normal"/>
            </rim:Name>
          </rim:Classification>
         */

        OMElement element = factory.createOMElement("Classification", ns);
        if (classificationScheme != null) {
            element.addAttribute("classificationScheme", classificationScheme, null);
        }
        if (classifiedObject != null) {
            element.addAttribute("classifiedObject", classifiedObject, null);
        }
        if (id != null) {
            element.addAttribute("id", id, null);
        }
        if (nodeRepresentation != null) {
            element.addAttribute("nodeRepresentation", nodeRepresentation, null);
        }
        if (objectType != null) {
            element.addAttribute("objectType", objectType, null);
        }

        element.addChild(createSlot("codingScheme",codingScheme,factory,ns));
        element.addChild(createName(nameValue, factory, ns));

        return element;
    }

    public OMElement createName(
            String nameValue,
            SOAPFactory factory,
            OMNamespace ns) {

        /*
        <rim:Name>
                    <rim:LocalizedString value="XDSDocument Entry 1"></rim:LocalizedString>
                </rim:Name>
         */

        OMElement name = factory.createOMElement("Name",ns);
        OMElement localizedString = factory.createOMElement("LocalizedString",ns);
        localizedString.addAttribute("value", nameValue, null);
        name.addChild(localizedString);
        return name;
    }

    public OMElement createSlot(
            String slotName,
            String valueListText,
            SOAPFactory factory,
            OMNamespace ns
    ) {

        /*
        <rim:Slot name="languageCode">
                    <rim:ValueList>
                        <rim:Value>en-us</rim:Value>
                    </rim:ValueList>
                </rim:Slot>
         */
        OMElement element = factory.createOMElement("Slot", ns);
        element.addAttribute("name", slotName, null);
        OMElement valueList = factory.createOMElement("ValueList",ns);
        OMElement value = factory.createOMElement("Value",ns);
        value.setText(valueListText);
        valueList.addChild(value);
        element.addChild(valueList);
        return element;
    }

    public OMElement createExtrinsicObject(String id,
                                           String mimeType,
                                           String objectType,
                                           String status,
                                           String creationTime,
                                           String patientId,
                                           SOAPFactory factory,
                                           OMNamespace ns) {
        /*
        <rim:ExtrinsicObject id="urn:uuid:4fe5af4f-f564-44cc-beef-0c742296c48f"
        mimeType="text/plain"
        objectType="urn:uuid:7edca82f-054d-47f2-a032-9b2a5b5186c1"
        status="urn:oasis:names:tc:ebxml-regrep:StatusType:Approved">
                <rim:Slot name="creationTime">
                    <rim:ValueList>
                        <rim:Value>20150412112333</rim:Value>
                    </rim:ValueList>
                </rim:Slot>
                <rim:Slot name="languageCode">
                    <rim:ValueList>
                        <rim:Value>en-us</rim:Value>
                    </rim:ValueList>
                </rim:Slot>
                <rim:Slot name="sourcePatientId">
                    <rim:ValueList>
                        <rim:Value>daac09bb0a3f45e^^^&amp;1.3.6.1.4.1.21367.2005.13.20.3000&amp;ISO</rim:Value>
                    </rim:ValueList>
                </rim:Slot>
                <rim:Name>
                    <rim:LocalizedString value="XDSDocument Entry 1"></rim:LocalizedString>
                </rim:Name>
         */

        OMElement element = factory.createOMElement("ExtrinsicObject", ns);
        if (id != null) {
            element.addAttribute("id", id, null);
        }
        if (mimeType != null) {
            element.addAttribute("mimeType", mimeType, null);
        }
        if (id != null) {
            element.addAttribute("id", id, null);
        }
        if (objectType != null) {
            element.addAttribute("objectType", objectType, null);
        }
        if (status != null) {
            element.addAttribute("status", status, null);
        }

        element.addChild(createSlot("creationTime",creationTime,factory,ns));
        element.addChild(createSlot("languageCode","lang-us",factory,ns));
        element.addChild(createSlot("sourcePatientId",patientId,factory,ns));
        element.addChild(createName("XDSDocument Entry 1", factory, ns));

        return element;
    }

    OMElement createAssociation(String associationType,
                                String id,
                                String sourceObject,
                                String targetObject,
                                String submissionSetStatus,
                                SOAPFactory factory,
                                OMNamespace ns){
        /*
        <rim:Association
        associationType="urn:oasis:names:tc:ebxml-regrep:AssociationType:HasMember"
        id="21b50cd2-2cbf-4cea-93e9-eb20525ab23a"
        sourceObject="urn:uuid:cc86a3f0-0787-4d81-8985-d5719428bc1b"
        targetObject="urn:uuid:4fe5af4f-f564-44cc-beef-0c742296c48f">
                <rim:Slot name="SubmissionSetStatus">
                    <rim:ValueList>
                        <rim:Value>Original</rim:Value>
                    </rim:ValueList>
                </rim:Slot>
            </rim:Association>
         */

        OMElement element = factory.createOMElement("Association", ns);
        if (associationType != null) {
            element.addAttribute("associationType", associationType, null);
        }
        if (id != null) {
            element.addAttribute("id", id, null);
        }
        if (sourceObject != null) {
            element.addAttribute("sourceObject", sourceObject, null);
        }
        if (targetObject != null) {
            element.addAttribute("targetObject", targetObject, null);
        }
        element.addChild(createSlot("SubmissionSetStatus",submissionSetStatus,factory,ns));
        return element;
    }

}



//InputStream in = new FileInputStream("/tmp/template.xml");
//OMElement registryObject = OMXMLBuilderFactory.createOMBuilder(in).getDocumentElement();
//registryObject.declareNamespace("urn:oasis:names:tc:ebxml-regrep:xsd:rim:3.0","rim");
                /*try {
                    logger.info("XML:" + registryObject.toStringWithConsume());
                } catch (Exception e) {
                    logger.info("While getting xml in file: "+e);
                }*/