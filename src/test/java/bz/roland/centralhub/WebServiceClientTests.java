package bz.roland.centralhub;

import bz.roland.centralhub.WebServiceClient;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Created by lars on 03.06.15.
 */
public class WebServiceClientTests  {

    private static final Log logger = LogFactory.getLog(WebServiceClientTests.class);

    @Test
    public void simpleSoapRequestTest()
    {
        String patientID = "daac09bb0a3f45e^^^&amp;1.3.6.1.4.1.21367.2005.13.20.3000&amp;ISO";

        WebServiceClient client = new WebServiceClient();
        //client.setDefaultUri("http://xdstest1.cloudapp.net/axis2/services/xdsrepositoryb.xdsrepositorybHttpSoap12Endpoint/");
        client.setDefaultUri("http://xdstest1.cloudapp.net:8020/axis2/services/xdsrepositoryb?wsdl");
        String ret = client.simpleSendAndReceive(
              patientID,
                "d:\\test.txt",
                "text/xml"
        );

        logger.info("simpleSendAndReceive returned " + ret);

        assert(ret.contains("Success"));
    }

}
